#!/bin/bash

APP=${APP:-"scs-cadvisor"}

set -x # 
cat <<EOT | kubectl apply -n web --force -f -
apiVersion: apps/v1
kind: DaemonSet
metadata:
  name: $APP
  namespace: web
  labels:
    app: $APP
spec:
  selector:
    matchLabels:
      app: $APP
  template:
    metadata:
      labels:
        app: $APP
    spec:
      tolerations:
      - key: node-role.kubernetes.io/master
        effect: NoSchedule
      hostNetwork: true
      containers:
      - name: $APP
        image: gitlab.scsuk.net:5005/scs-systems/ext_registry/google/cadvisor:v0.33.0
        ports:
        - containerPort: 8080
        volumeMounts:
        - name: rootfs
          mountPath: /rootfs
          readOnly: true
        - name: var-run
          mountPath: /var/run
          readOnly: false
        - name: sys
          mountPath: /sys
          readOnly: true
        - name: docker
          mountPath: /var/lib/docker  #Mouting Docker volume  
          readOnly: true
        - name: disk
          mountPath: /dev/disk
          readOnly: true
      volumes:
      - name: rootfs
        hostPath:
          path: /
      - name: var-run
        hostPath:
          path: /var/run
      - name: sys
        hostPath:
          path: /sys
      - name: docker
        hostPath:
          path: /var/lib/docker #Docker path in Host System
      - name: disk
        hostPath:
          path: /dev/disk
EOT

echo
kubectl -n web get ds $APP
echo
kubectl -n web get pods -l app=$APP -o wide
